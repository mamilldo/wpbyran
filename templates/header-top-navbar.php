<div id="nav-wrapper">
    <header class="banner navbar navbar-default " role="banner" id="banner" >
      <!-- <div class="container"> -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="glyphicon glyphicon-th-list"></span>

          </button>

          <a href="<?php echo home_url(); ?>">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/wpb-logo-small.png" alt="WP_Byrån" class="img-responsive">
                <span class="sr-only"><?php bloginfo('name'); ?></span>
            </a>

        </div>

        <nav class="collapse navbar-collapse" role="navigation">
          <?php
            if (has_nav_menu('primary_navigation')) :
              wp_nav_menu(array('theme_location' => 'primary_navigation', 'menu_class' => 'nav navbar-nav '));
            endif;
          ?>
        </nav>
     <!--  </div> -->
    </header>
</div>

