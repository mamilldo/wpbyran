<?php while (have_posts()) : the_post(); ?>
<?php while(has_sub_field("content")): ?>

    <?php if(get_row_layout() == "full_width_text_container"): // layout: Content

        $printid = '';
        $id = get_sub_field("section_id");
        if ( "" !== $id  ) {
            $id = get_sub_field("section_id");
            $printid = ' id="'.  $id . '"';
        } ?>
        <section class="section <?php the_sub_field("bg_color"); ?>"<?php echo $printid; ?>>
            <div class="container">
                <div class="row">
                    <h1 class="text-center"><?php the_sub_field("header"); ?></h1>
                    <div class="col-sm-12">
                        <?php the_sub_field("full_width_text"); ?>
                    </div>

                    <?php //if kontakt-section *********************************
                    if ( 'kontakt' == $id ) { ?>
                    <div class="col-sm-8">
                    <?php echo do_shortcode( '[gravityform id="1" title="false" description="false" ajax="true"]' ); ?>
                        </div>
                        <div class="col-sm-4">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/wpb-logo.png" class="img-responsive hidden-xs">
                        </div>
                    <?php
                    } // end if kontakt-section *********************************
                    ?>

                </div><!-- // row -->
            </div> <!-- //container -->
                <?php // if full_width_text_container has sub items eg. personal etc
                        $items = get_sub_field('items');
                        if ( get_sub_field('items') )  {
                            $itemnumber = '0';
                            $itemnumberforid = 1;
                            $fulltextitem_html = '';

                            foreach( $items as $item ):
                            if ( 0 == $itemnumber ) {
                                echo '<div class="container"><div class="row">';
                            }
                            $headerlink = '';
                            if ( "" !== $item['email'] ) {
                                if (strpos($item['email'],'@') !== false) {
                                    $headerlink = ' <small><strong><a href="mailto:' . $item['email'] . '">' . $item['email'] . '</a></strong></small>';
                                } else {
                                    $headerlink = ' <small><strong><a href="' . $item['email'] . '">' . $item['email'] . '</a></strong></small>';
                                }
                            }
                            $fulltextitem_html .= '<div class="lightgray folded fade" id="' . $id . '-' . $itemnumberforid . '"><div class="container"><div class="row margin-bottom-large"><div class="col-sm-4">';
                            $fulltextitem_html .= '<img src="' . $item['image']['sizes']['large'] . '" class="img-responsive  margin-top-large"></div>';
                            $fulltextitem_html .= '<div class="col-sm-8">';
                            $fulltextitem_html .= '<h3>' . $item['header'] . $headerlink . '</h3>';
                            $fulltextitem_html .= '<h4>' . $item['short_text'] . '</h4>';
                            $fulltextitem_html .= $item['text'] ;
                            $fulltextitem_html .= '</div></div></div></div>';
                            ?>
                                    <div class="col-sm-6 col-md-3">
                                        <a class="showmore"  data-parent="#<?php echo $id; ?>" data-target="#<?php echo $id; ?>-<?php echo $itemnumberforid; ?>">
                                        <div class="text-center">
                                            <img src="<?php echo $item['image']['sizes']['thumbnail'];?>" class="img-circle img-responsive">
                                                <h5><?php echo $item['header']; ?></h5>
                                                <p class="small"><?php echo $item['short_text']; ?></p>
                                        </div>
                                        </a>
                                    </div>

                            <?php
                            $itemnumber = $itemnumber +1;
                            $itemnumberforid = $itemnumberforid + 1;
                            if ( 4 == $itemnumber ) {
                                echo '</div></div>';
                                echo $fulltextitem_html;
                                $fulltextitem_html = '';
                                ?>

                         <?php
                                $itemnumber = 0;
                            }
                            endforeach;
                            if ( $itemnumber < 4 ) {
                                echo "</div></div>";
                                echo $fulltextitem_html;
                            }
                            ?>


                        <?php
                        } // end if sub items  ?>

        </section>

        <?php // Carousel
            elseif(get_row_layout() == "carousel"): ?>
                <?php $items = get_sub_field('carousel_item');
                    $itemnumber = '0';
                    $indicators_html = '';
                    $html_item = '';
                foreach( $items as $item ):
                    $url_start = "";
                    $url_end = "";
                    $more_button = "";
                    $read_more_text = "";
                    $watermark = "";
                    $read_more_text =$item['read_more_text'];
                    if(  !(empty($item['page_link'])) || !(empty($item['manual_link']))  ) {
                            $more_button = '<p class="hidden-xs"><button class="btn btn-xs btn-primary pull-right">Learn more</button></p>';
                        }
                    if ( !(empty($item['page_link'])) ) {
                        $url_start = '<a href="' . $item['page_link'] . '">';
                        $url_end = '</a>';
                    } elseif ( !(empty($item['manual_link'])) ) {
                        $url_start = '<a href="' . $item['manual_link'] . '">';
                        $url_end = '</a>';
                    }
                    if( 0 == $itemnumber ) {
                        $active = "active";
                    } else {
                        $active = "";
                    }
                     // if ( !(empty($item['watermark'])) ) {
                     //    $watermark = '<img src="' . get_template_directory_uri() . '/assets/img/epn-logo-' . $item['watermark'] . '.png" class="watermark img-responsiveg">';
                     // }
                    $indicators_html .= '<li data-target="#mycarousel" data-slide-to="' . $itemnumber . '" class="' . $active . '"><small>' . $read_more_text  .'</small></li>';

                        $html_item .= '<div class="item ' . $active .'" style="background-image: url(' . $item['image']['sizes']['container-lg'] . ')">';
                          $html_item .= $watermark;
                          $html_item .= $url_start . '<div class="carousel-caption">';
                            $html_item .= '<h3>' . $item['header'] . '</h3>';
                            $html_item .= '<p>' . $item['body_text'] . '</p>' . $more_button;
                          $html_item .= '</div>' . $url_end;
                        $html_item .= '</div>';
                $itemnumber = $itemnumber +1;
                endforeach; ?>
                <div id="mycarousel" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <?php echo $indicators_html; ?>
                    </ol>
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                        <?php echo $html_item; ?>
                    </div>
                    <!-- Controls -->
                    <a class="left carousel-control" href="#mycarousel" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left"></span>
                    </a>
                    <a class="right carousel-control" href="#mycarousel" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                    </a>
                </div>
<?php // Two columns, text and image ?>
    <?php elseif(get_row_layout() == "two_columns_text_and_image"): // layout: Content ?>
        <?php
            $printid = get_sub_field("section_id");
            if ( "" !== $printid ) {
                $printid = ' id="'.  get_sub_field("section_id") . '"';
            } ?>
        <section class="section <?php the_sub_field("bg_color"); ?>"<?php echo $printid; ?>>
            <div class="container">
                <div class="row">
                <?php
                //Använder bootstrap push & pull för att ändra ordning, bild alltid överst på mobilen
                $align = get_sub_field("left_right");
                $image = get_sub_field('image');
                if ( $align == "right" ) {
                    $push = "col-sm-push-6";
                    $pull = "col-sm-pull-6";
                    }?>
                    <h1 class="text-center"><?php the_sub_field("header"); ?></h1>
                    <div class="col-sm-6 <?php echo $push; ?>">
                        <img class="img-rounded img-responsive" src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>">
                    </div>
                    <div class="col-sm-6 <?php echo $pull; ?>">
                        <?php the_sub_field("text"); ?>
                    </div>

                </div>
            </div>
        </section>
    <?php endif; ?>

<?php endwhile; //end content layouts ?>
<?php endwhile; ?>
