/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 *
 * Google CDN, Latest jQuery
 * To use the default WordPress version of jQuery, go to lib/config.php and
 * remove or comment out: add_theme_support('jquery-cdn');
 * ======================================================================== */

(function($) {

// Use this variable to set up the common and page specific functions. If you
// rename this variable, you will also need to rename the namespace below.
var Roots = {
  // All pages
  common: {
    init: function() {
      // JavaScript to be fired on all pages
      $('#banner').affix({
        offset: { top: $('#banner').offset().top }
      });
      $('#nav-wrapper').height($("#banner").height());

      if ($("body").hasClass("home")) {
            // gör inget
        }
        else {
            $("#menu-primary-navigation a, a.navbar-brand").each(function() {
                this.href = "/" + this.hash;
            });
        }
        $('a[href^="#"]').on('click',function (e) {
          //e = $.event.fix(e);
          //e.preventDefault();
          if(e.preventDefault) {
              e.preventDefault();
          }
          var target = this.hash,
          $target = $(target);
          var currentHeight = $('#banner').height();
          var currentWidth = $('#banner').width();
          if(currentWidth <= 760) {
              currentHeight = 50;
              $('.navbar-collapse').collapse('hide');
          }
          $('html, body').stop().animate({
              'scrollTop': $target.offset().top - currentHeight
          }, 400, 'swing', function () {
              if (typeof(window.history.pushState) === 'function') {
                  window.history.pushState(null, target, target);
              } else {
                  window.location.hash = '#!' + target;
              }
              return false;
          });
      });

     $('a.showmore').on('click',function (e) {
        if(e.preventDefault) {
              e.preventDefault();
          }
        var current = $(this).data('target'),
            bannerHeight = $('#banner').height(),
            currentclicked = $(this);

        bannerHeight = bannerHeight+90;
        if ( $('.folded').hasClass('in') ) {
            $(".folded.in").slideUp(400,function(){
                $('.folded.in').removeClass('in');

                $(current).slideDown(500,function(){
                    $(current).addClass('in').fadeIn(600);
                    $('a.showmore').removeClass('active');
                    $(currentclicked).addClass('active');
                    $.scrollTo( current, 600, {offset:{top:-bannerHeight,left:0}} );
                });
            });
        } else {
                $(current).slideDown(500,function(){
                    $(current).addClass('in').fadeIn(600);
                    $('a.showmore').removeClass('active');
                    $(currentclicked).addClass('active');
                    $.scrollTo( current, 600, {offset:{top:-bannerHeight,left:0}} );
                });
        }


     });
    }
  },
  // Home page
  home: {
    init: function() {
      // JavaScript to be fired on the home page
      $('form#contactForm').submit(function() {
        $('form#contactForm .error').remove();
        var hasError = false;
        $('.requiredField').each(function() {
            if(jQuery.trim($(this).val()) === '') {
                var labelText = $(this).prev('label').text();
                $(this).parent().append('<span class="text-danger">Du glömde bort att ange '+labelText+'.</span>');
                hasError = true;
            } else if($(this).hasClass('email')) {
                var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
                if(!emailReg.test(jQuery.trim($(this).val()))) {
                    var labelTextEmail = $(this).prev('label').text();
                    $(this).parent().append('<span class="text-danger">Kontrollera din angivna epostadress.</span>');
                    hasError = true;
                }
            }
        });
        if(!hasError) {
            $('form#contactForm button').fadeOut('normal', function() {
                $(this).parent().append('<img src="/wp-content/themes/wpbyran/assets/img/loading.gif" alt="Skickar&hellip;" height="31" width="31" />');
            });
            var formInput = $(this).serialize();
            $.post($(this).attr('action'),formInput, function(data){
                console.log(formInput);
                $('form#contactForm').hide("slow", function() {
                    $(this).before('<h1  class="text-success text-center">Tack!</h1> <h4 class="text-success text-center">Ditt meddelande har skickats. Vi återkommer inom kort till dig. </h4><h3 class="text-success text-center">Ha en härlig dag!</h3>');
                    window.location.hash = '#kontakt';
                });
            });
        }

        return false;

    });
    }
  },
  // About us page, note the change from about-us to about_us.
  about_us: {
    init: function() {
      // JavaScript to be fired on the about us page
    }
  }
};

// The routing fires all common scripts, followed by the page specific scripts.
// Add additional events for more control over timing e.g. a finalize event
var UTIL = {
  fire: function(func, funcname, args) {
    var namespace = Roots;
    funcname = (funcname === undefined) ? 'init' : funcname;
    if (func !== '' && namespace[func] && typeof namespace[func][funcname] === 'function') {
      namespace[func][funcname](args);
    }
  },
  loadEvents: function() {
    UTIL.fire('common');

    $.each(document.body.className.replace(/-/g, '_').split(/\s+/),function(i,classnm) {
      UTIL.fire(classnm);
    });
  }
};

$(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
